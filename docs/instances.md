# Instances

* <https://scribe.rip> (official)
* <https://scribe.nixnet.services>
* <https://scribe.citizen4.eu>
* <https://scribe.bus-hit.me>
* <https://scribe.froth.zone>
* <https://scribe.esmailelbob.xyz>
* <https://scribe.privacydev.net>
* <https://scribe.rawbit.ninja>
* <http://scribe.esmail5pdn24shtvieloeedh7ehz3nrwcdivnfhfcedl7gf4kwddhkqd.onion> (Tor)
* <http://w7uhv5lxhgck72hhimdglmusc54t4m6bionlmd5mvyddq3bs53mohqid.onion> (Tor)
* [sc.vern.i2p](http://vern3whzyfmjclq6snhlupma6nrmojghwp37tydfgqotj7sc6izq.b32.i2p) (I2P)

## How do I get my instance on this list?

Check out the [Contributing portion of the README](../README.md#contributing) and submit a patch to this file.

## How do I start my own instance?

There's no documentation at this time. If you have some server know-how, give it a shot. If you run into trouble, feel free to contact the [mailing list](https://lists.sr.ht/~edwardloveall/scribe) for help.
